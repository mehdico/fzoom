import Flutter
import UIKit
import MobileRTC
import Foundation
import CommonCrypto

public class SwiftFzoomPlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        let factory = ZoomViewFactory(messenger: registrar.messenger())
        registrar.register(factory, withId: "zoom_view")
    }
}


public class ZoomViewFactory: NSObject, FlutterPlatformViewFactory {
    
    private weak var messenger: (NSObjectProtocol & FlutterBinaryMessenger)?
    
    init(messenger: (NSObjectProtocol & FlutterBinaryMessenger)?) {
        self.messenger = messenger
        super.init()
    }
    
    public func create(
        withFrame frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?
    ) -> FlutterPlatformView {
        return ZoomView(frame, viewId: viewId, messenger: messenger, args: args)
    }
}

public class AuthenticationDelegate: NSObject, MobileRTCAuthDelegate {
    
    private var result: FlutterResult?
    
    
    public func onAuth(_ result: FlutterResult?) -> AuthenticationDelegate {
        self.result = result
        return self
    }
    
    
    public func onMobileRTCAuthReturn(_ returnValue: MobileRTCAuthError) {
        if self.result != nil {
            if returnValue == MobileRTCAuthError_Success {
                self.result?([0, 0])
            } else {
                self.result?([1, 0])
            }
            self.result = nil
        }
    }
    
    public func onMobileRTCLoginReturn(_ returnValue: Int) {
        if self.result != nil {
            self.result?(returnValue)
            self.result = nil
        }
    }
    
    public func onMobileRTCLogoutReturn(_ returnValue: Int) {
        if self.result != nil {
            self.result?(returnValue)
            self.result = nil
        }
    }
    
    public func getAuthErrorMessage(_ errorCode: MobileRTCAuthError) -> String {
        let message = ""
        
        //         switch (errorCode) {
        //             case MobileRTCMeetError_Success:
        //                 message = "Successfully start/join meeting."
        //                 break
        //             case MobileRTCMeetError_NetworkError:
        //                 message = "Network issue, please check your network connection."
        //                 break
        //             case MobileRTCMeetError_ReconnectError:
        //                 message = "Failed to reconnect to meeting."
        //                 break
        //             case MobileRTCMeetError_MMRError:
        //                 message = "MMR issue, please check mmr configruation."
        //                 break
        //             case MobileRTCMeetError_PasswordError:
        //                 message = "Meeting password incorrect."
        //                 break
        //             case MobileRTCMeetError_SessionError:
        //                 message = "Failed to create a session with our sever."
        //                 break
        //             case MobileRTCMeetError_MeetingOver:
        //                 message = "The meeting is over."
        //                 break
        //             case MobileRTCMeetError_MeetingNotStart:
        //                 message = "The meeting does not start."
        //                 break
        //             case MobileRTCMeetError_MeetingNotExist:
        //                 message = "The meeting does not exist."
        //                 break
        //             case MobileRTCMeetError_MeetingUserFull:
        //                 message = "The meeting has reached a maximum of participants."
        //                 break
        //             case MobileRTCMeetError_MeetingClientIncompatible:
        //                 message = "The Zoom SDK version is incompatible."
        //                 break
        //             case MobileRTCMeetError_NoMMR:
        //                 message = "No mmr is available at this point."
        //                 break
        //             case MobileRTCMeetError_MeetingLocked:
        //                 message = "The meeting is locked by the host."
        //                 break
        //             case MobileRTCMeetError_MeetingRestricted:
        //                 message = "The meeting is restricted."
        //                 break
        //             case MobileRTCMeetError_MeetingRestrictedJBH:
        //                 message = "The meeting does not allow join before host. Please try again later."
        //                 break
        //             case MobileRTCMeetError_CannotEmitWebRequest:
        //                 message = "Failed to send create meeting request to server."
        //                 break
        //             case MobileRTCMeetError_CannotStartTokenExpire:
        //                 message = "Failed to start meeting due to token exipred."
        //                 break
        //             case MobileRTCMeetError_VideoError:
        //                 message = "The user's video cannot work."
        //                 break
        //             case MobileRTCMeetError_AudioAutoStartError:
        //                 message = "The user's audio cannot auto start."
        //                 break
        //             case MobileRTCMeetError_RegisterWebinarFull:
        //                 message = "The webinar has reached its maximum allowed participants."
        //                 break
        //             case MobileRTCMeetError_RegisterWebinarHostRegister:
        //                 message = "Sign in to start the webinar."
        //                 break
        //             case MobileRTCMeetError_RegisterWebinarPanelistRegister:
        //                 message = "Join the webinar from the link"
        //                 break
        //             case MobileRTCMeetError_RegisterWebinarDeniedEmail:
        //                 message = "The host has denied your webinar registration."
        //                 break
        //             case MobileRTCMeetError_RegisterWebinarEnforceLogin:
        //                 message = "The webinar requires sign-in with specific account to join."
        //                 break
        //             case MobileRTCMeetError_ZCCertificateChanged:
        //                 message = "The certificate of ZC has been changed. Please contact Zoom for further support."
        //                 break
        //             case MobileRTCMeetError_VanityNotExist:
        //                 message = "The vanity does not exist"
        //                 break
        //             case MobileRTCMeetError_JoinWebinarWithSameEmail:
        //                 message = "The email address has already been register in this webinar."
        //                 break
        //             case MobileRTCMeetError_WriteConfigFile:
        //                 message = "Failed to write config file."
        //                 break
        //             case MobileRTCMeetError_RemovedByHost:
        //                 message = "You have been removed by the host."
        //                 break
        //             case MobileRTCMeetError_InvalidArguments:
        //                 message = "Invalid arguments."
        //                 break
        //             case MobileRTCMeetError_InvalidUserType:
        //                 message = "Invalid user type."
        //                 break
        //             case MobileRTCMeetError_InAnotherMeeting:
        //                 message = "Already in another ongoing meeting."
        //                 break
        //             case MobileRTCMeetError_Unknown:
        //                 message = "Unknown error."
        //                 break
        //             default:
        //                 message = "Unknown error."
        //                 break
        //         }
        return message
    }
}

public class ZoomView: NSObject, FlutterPlatformView, MobileRTCMeetingServiceDelegate, FlutterStreamHandler {
    let frame: CGRect
    let viewId: Int64
    var channel: FlutterMethodChannel
    var authenticationDelegate: AuthenticationDelegate
    
    var statusEventChannel: FlutterEventChannel
    var eventSink: FlutterEventSink?

    let ERROR_INIT_FAILED = -1;
    let ERROR_SERVICE_NULL = -2;
    let ERROR_USER_INFO_NULL = -3;
    let ERROR_LOGIN = -4;
    let ERROR_LOGOUT = -5;
    let ERROR_MEETING_NOT_FOUND = -6;
    let ERROR_MEETING_START = -7;
    let ERROR_MEETING_JOIN = -8;
    let SUCCESS = 0;
    
    init(_ frame: CGRect, viewId: Int64, messenger: (NSObjectProtocol & FlutterBinaryMessenger)?, args: Any?) {
        self.frame = frame
        self.viewId = viewId
        self.channel = FlutterMethodChannel(name: "zoom_view", binaryMessenger: messenger!)
        self.authenticationDelegate = AuthenticationDelegate()
        self.statusEventChannel = FlutterEventChannel(name: "zoom_event_stream" + String(format: "_%lld", viewId), binaryMessenger: messenger!)
        
        super.init()
        
        self.statusEventChannel.setStreamHandler(self)
        self.channel.setMethodCallHandler(self.onMethodCall)
    }
    
    public func view() -> UIView {
        
        let label = UILabel(frame: frame)
        label.text = ""
        return label
    }
    
    public func onMethodCall(call: FlutterMethodCall, result: @escaping FlutterResult) {
        NSLog("ios received call.method: %@", call.method)
        switch call.method {
        case "init":
            self.initZoom(call: call, result: result)
        case "joinMeetingNoLogin":
            self.joinMeetingNoLogin(call: call, result: result)
        case "login":
            self.login(call: call, result: result)
        case "logout":
            self.logout(call: call, result: result)
        case "startMeeting":
            self.startMeeting(call: call, result: result)
        case "startMeetingNoLogin":
            self.startMeetingNoLogin(call: call, result: result)
        case "meetingStatus":
            self.meetingStatus(call: call, result: result)
        case "currentMeetingInviteInfo":
            self.currentMeetingInviteInfo(call: call, result: result)
        default:
            result(FlutterMethodNotImplemented)
        }
    }
    
    public func initZoom(call: FlutterMethodCall, result: @escaping FlutterResult)  {
        
        let pluginBundle = Bundle(for: type(of: self))
        let pluginBundlePath = pluginBundle.bundlePath
        let arguments = call.arguments as! Dictionary<String, String?>
        
        let context = MobileRTCSDKInitContext()
        context.domain = arguments["domain"]!
        context.enableLog = true
        context.bundleResPath = pluginBundlePath
        MobileRTC.shared().initialize(context)
        
        let auth = MobileRTC.shared().getAuthService()
        auth?.delegate = self.authenticationDelegate.onAuth(result)
        auth?.clientKey = arguments["appKey"]!
        auth?.clientSecret = arguments["appSecret"]!
        auth?.sdkAuth()
    }
    
    
    public func meetingStatus(call: FlutterMethodCall, result: @escaping FlutterResult) {
        
        let meetingService = MobileRTC.shared().getMeetingService()
        if meetingService != nil {
            
            let meetingState = meetingService?.getMeetingState()
            result(getStateMessage(meetingState))
        } else {
            result(["MEETING_STATUS_UNKNOWN", "Unknown error"])
        }
    }
    
    public func joinMeetingNoLogin(call: FlutterMethodCall, result: FlutterResult) {
        
        let meetingService = MobileRTC.shared().getMeetingService()
        let meetingSettings = MobileRTC.shared().getMeetingSettings()
        
        if meetingService == nil {
            result(self.ERROR_SERVICE_NULL)
            return
        }
        let arguments = call.arguments as! Dictionary<String, String?>
        meetingSettings?.disableDriveMode(parseBoolean(data: arguments["disableDrive"]!, defaultValue: false))
        meetingSettings?.disableCall(in: parseBoolean(data: arguments["disableDialIn"]!, defaultValue: false))
        meetingSettings?.setAutoConnectInternetAudio(parseBoolean(data: arguments["noDisconnectAudio"]!, defaultValue: false))//todo check if it is the right param
        meetingSettings?.setMuteAudioWhenJoinMeeting(parseBoolean(data: arguments["noAudio"]!, defaultValue: false))
        meetingSettings?.meetingShareHidden = parseBoolean(data: arguments["disableShare"]!, defaultValue: false)
        meetingSettings?.meetingInviteHidden = parseBoolean(data: arguments["disableInvite"]!, defaultValue: false)
        
        var params = [
            kMeetingParam_Username: arguments["displayName"]!!,
            kMeetingParam_MeetingNumber: arguments["meetingId"]!!
        ]
        
        let hasPassword = arguments["meetingPassword"]! != nil
        if hasPassword {
            params[kMeetingParam_MeetingPassword] = arguments["meetingPassword"]!!
        }
        
        let response = meetingService?.joinMeeting(with: params)
        
        result(response?.rawValue ?? self.ERROR_MEETING_JOIN)
    }
    
    public func login(call: FlutterMethodCall, result: @escaping FlutterResult) {
        let arguments = call.arguments as! Dictionary<String, String?>
        guard let userId = arguments["userId"] as? String, let password = arguments["password"] as? String else {
            result(self.ERROR_LOGIN)
            return
        }
        
        let auth = MobileRTC.shared().getAuthService()
        auth?.delegate = self.authenticationDelegate.onAuth(result)
        
        MobileRTC.shared().getAuthService()?.login(withEmail: userId,
                                                   password: password,
                                                   rememberMe: false)
    }
    
    public func logout(call: FlutterMethodCall, result: @escaping FlutterResult) {
        let auth = MobileRTC.shared().getAuthService()
        auth?.delegate = self.authenticationDelegate.onAuth(result)
        
        auth?.logoutRTC()
        result(self.SUCCESS)
    }
    
    public func startMeeting(call: FlutterMethodCall, result: FlutterResult) {
        
        let meetingService = MobileRTC.shared().getMeetingService()
        let meetingSettings = MobileRTC.shared().getMeetingSettings()
        
        if meetingService == nil {
            result(self.ERROR_SERVICE_NULL)
            return
        }
            
        let arguments = call.arguments as! Dictionary<String, String?>
        
        meetingSettings?.meetingInviteHidden = parseBoolean(data: arguments["disableInvite"]!, defaultValue: false)
        meetingSettings?.meetingShareHidden = parseBoolean(data: arguments["disableShare"]!, defaultValue: false)
        meetingSettings?.disableDriveMode(parseBoolean(data: arguments["disableDrive"]!, defaultValue: false))
        meetingSettings?.disableCall(in: parseBoolean(data: arguments["disableDialIn"]!, defaultValue: false))
        meetingSettings?.setAutoConnectInternetAudio(parseBoolean(data: arguments["noDisconnectAudio"]!, defaultValue: false))//todo
        meetingSettings?.meetingVideoHidden = parseBoolean(data: arguments["noVideo"]!, defaultValue: false)
        
        let user = MobileRTCMeetingStartParam4LoginlUser()
        //            user.meetingNumber
        
        //                let params = [
        //                    kMeetingParam_UserID: arguments["userId"]!!,
        //                    kMeetingParam_Username: arguments["userId"],
        //                    kMeetingParam_Username: "123",
        //                    kMeetingParam_MeetingNumber: arguments["meetingId"]!!,
        //                ]
        
        let response = meetingService?.startMeeting(with: user)
        
        result(response?.rawValue ?? self.ERROR_MEETING_START)
    }
    
    public func startMeetingNoLogin(call: FlutterMethodCall, result: @escaping FlutterResult) {
        
        let meetingService = MobileRTC.shared().getMeetingService()
        let meetingSettings = MobileRTC.shared().getMeetingSettings()
        
        if meetingService == nil {
            result(self.ERROR_SERVICE_NULL)
            return
        }
            
        let arguments = call.arguments as! Dictionary<String, String?>
        
        meetingSettings?.meetingInviteHidden = parseBoolean(data: arguments["disableInvite"]!, defaultValue: false)
        meetingSettings?.meetingShareHidden = parseBoolean(data: arguments["disableShare"]!, defaultValue: false)
        meetingSettings?.disableDriveMode(parseBoolean(data: arguments["disableDrive"]!, defaultValue: false))
        meetingSettings?.disableCall(in: parseBoolean(data: arguments["disableDialIn"]!, defaultValue: false))
        meetingSettings?.setAutoConnectInternetAudio(parseBoolean(data: arguments["noDisconnectAudio"]!, defaultValue: false))
        meetingSettings?.meetingVideoHidden = parseBoolean(data: arguments["noVideo"]!, defaultValue: false)
        
        
        let userId = arguments["userId"]!!
        let apiKey = arguments["apiKey"]!!
        let apiSecret = arguments["apiSecret"]!!
        
        guard let token = requestTokenOrZAK(isTokenType: true, userId: userId, apiKey: apiKey, apiSecret: apiSecret)else {
            result(self.ERROR_USER_INFO_NULL)
            return
        }
        
        guard let zak = requestTokenOrZAK(isTokenType: false, userId: userId, apiKey: apiKey, apiSecret: apiSecret) else {
            result(self.ERROR_USER_INFO_NULL)
            return
        }
        
        let user = MobileRTCMeetingStartParam4WithoutLoginUser()
        user.userID = userId
        user.userName = arguments["displayName"]!!
        user.userToken = token
        user.zak = zak
        user.userType = MobileRTCUserType_APIUser
        user.meetingNumber = arguments["meetingId"]!!
        
        let startParam : MobileRTCMeetingStartParam = user
        let response = meetingService?.startMeeting(with: startParam)
        
        result(response?.rawValue ?? self.ERROR_MEETING_START)
    }

    public func currentMeetingInviteInfo(call: FlutterMethodCall, result: @escaping FlutterResult) {
        
        let meetingService = MobileRTC.shared().getMeetingService()
        if meetingService == nil {
            result(self.ERROR_SERVICE_NULL)
            return
        }
        
        let inviteHelperService = MobileRTCInviteHelper.sharedInstance()
                
        let ongoingMeetingNumber = inviteHelperService.ongoingMeetingNumber
        if ongoingMeetingNumber.isEmpty {
            result([self.ERROR_MEETING_NOT_FOUND])
            return
        }
        
        result([
            SUCCESS,
            inviteHelperService.ongoingMeetingNumber,
            inviteHelperService.meetingPassword,
            inviteHelperService.joinMeetingURL
        ])
    }
    
    func requestTokenOrZAK(isTokenType: Bool, userId: String, apiKey: String, apiSecret: String) -> String? {
        let bodyString = "token?type=\(isTokenType ? "token" : "zak")&access_token=\(self.createJWTAccessToken(apiKey: apiKey, apiSecret: apiSecret) ?? "")"
        var urlString = "\("https://api.zoom.us/v2/users")/\(userId)/\(bodyString)"
        urlString = stringByAddingPercentEncodingToData(string: urlString)!
        
        
        guard let url = URL(string: urlString) else{
            return nil
        }
        
        let semaphore = DispatchSemaphore(value: 0)
        var dictionary: [String : Any]? = nil
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else {
                return
            }
            
            //let result = String(data: data, encoding: String.Encoding.utf8)!
            
            do {
                dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
            } catch {
                NSLog("parse failed")
            }
            semaphore.signal()
        }
        task.resume()
        semaphore.wait()
        
        return dictionary?["token"] as? String
    }
    
    
    func createJWTAccessToken(apiKey: String, apiSecret: String) -> String? {
        //    {
        //        "alg": "HS256",
        //        "typ": "JWT"
        //    }
        var dictHeader: [AnyHashable : Any] = [:]
        dictHeader["alg"] = "HS256"
        dictHeader["typ"] = "JWT"
        let base64Header = self.base64Encode(self.dictionary(toJson: dictHeader))
        //    {
        //        "iss": "API_KEY",
        //        "exp": 1496091964000
        //    }
        var dictPayload: [AnyHashable : Any] = [:]
        dictPayload["iss"] = apiKey
        dictPayload["exp"] = currentTimeInMilliSeconds() / 1000 + (3600 * 100) // time + 100 hours
        let base64Payload = self.base64Encode(self.dictionary(toJson: dictPayload))
        let composer = "\(base64Header ?? "").\(base64Payload ?? "")"
        let hashmac = hmac(string: composer, key: apiSecret)
        let accesstoken = "\(base64Header ?? "").\(base64Payload ?? "").\(hashmac)"
        
        return accesstoken
    }
    
    
    public func onMeetingError(_ error: MobileRTCMeetError, message: String?) {
        
    }
    
    public func getMeetErrorMessage(_ errorCode: MobileRTCMeetError) -> String {
        let message = ""
        //         switch (errorCode) {
        //             case MobileRTCAuthError_Success:
        //                 message = "Authentication success."
        //                 break
        //             case MobileRTCAuthError_KeyOrSecretEmpty:
        //                 message = "SDK key or secret is empty."
        //                 break
        //             case MobileRTCAuthError_KeyOrSecretWrong:
        //                 message = "SDK key or secret is wrong."
        //                 break
        //             case MobileRTCAuthError_AccountNotSupport:
        //                 message = "Your account does not support SDK."
        //                 break
        //             case MobileRTCAuthError_AccountNotEnableSDK:
        //                 message = "Your account does not support SDK."
        //                 break
        //             case MobileRTCAuthError_Unknown:
        //                 message = "Unknown error.Please try again."
        //                 break
        //             default:
        //                 message = "Unknown error.Please try again."
        //                 break
        //         }
        return message
    }
    
    public func onMeetingStateChange(_ state: MobileRTCMeetingState) {
        
        guard let eventSink = eventSink else {
            return
        }
        
        eventSink(getStateMessage(state))
    }
    
    public func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        self.eventSink = events
        
        let meetingService = MobileRTC.shared().getMeetingService()
        if meetingService == nil {
            return FlutterError(code: "Zoom SDK error", message: "ZoomSDK is not initialized", details: nil)
        }
        meetingService?.delegate = self
        
        return nil
    }
    
    public func onCancel(withArguments arguments: Any?) -> FlutterError? {
        eventSink = nil
        return nil
    }
    
    private func getStateMessage(_ state: MobileRTCMeetingState?) -> [String] {
        
        var message: [String]
        
        switch state {
        case MobileRTCMeetingState_Idle:
            message = ["MEETING_STATUS_IDLE", "No meeting is running"]
            break
        case MobileRTCMeetingState_Connecting:
            message = ["MEETING_STATUS_CONNECTING", "Connect to the meeting server"]
            break
        case MobileRTCMeetingState_InMeeting:
            message = ["MEETING_STATUS_INMEETING", "Meeting is ready and in process"]
            break
        case MobileRTCMeetingState_WebinarPromote:
            message = ["MEETING_STATUS_WEBINAR_PROMOTE", "Upgrade the attendees to panelist in webinar"]
            break
        case MobileRTCMeetingState_WebinarDePromote:
            message = ["MEETING_STATUS_WEBINAR_DEPROMOTE", "Demote the attendees from the panelist"]
            break
        default:
            message = ["MEETING_STATUS_UNKNOWN", "Unknown error"]
        }
        
        return message
    }
    
    func base64Encode(_ decodeString: String?) -> String? {
        let encodeData = decodeString?.data(using: .utf8)
        let base64String = encodeData?.base64EncodedString(options: [])
        
        var mutStr = base64String ?? ""
        let range = NSRange(location: 0, length: (base64String?.count ?? 0))
        if let subRange = Range<String.Index>(range, in: mutStr) { mutStr = mutStr.replacingOccurrences(of: "=", with: "", options: .literal, range: subRange) }
        
        return mutStr
    }
    
    func dictionary(toJson dict: Dictionary<AnyHashable, Any>) -> String? {
        var jsonData: Data? = nil
        do {
            jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
        } catch {
        }
        var jsonString: String?
        if jsonData == nil {
            return nil
        } else {
            if let jsonData = jsonData {
                jsonString = String(data: jsonData, encoding: .utf8)
            }
        }
        
        var mutStr = jsonString ?? ""
        let range = NSRange(location: 0, length: (jsonString?.count ?? 0))
        if let subRange = Range<String.Index>(range, in: mutStr) { mutStr = mutStr.replacingOccurrences(of: " ", with: "", options: .literal, range: subRange) }
        let range2 = NSRange(location: 0, length: mutStr.count)
        if let subRange = Range<String.Index>(range2, in: mutStr) { mutStr = mutStr.replacingOccurrences(of: "\n", with: "", options: .literal, range: subRange) }
        return mutStr
    }
    
    public func stringByAddingPercentEncodingToData(string: String) -> String? {
        let finalString = string.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)?.replacingOccurrences(of: "+", with: "%2B")
        return finalString
    }
    
    func currentTimeInMilliSeconds()-> Int
    {
        let currentDate = Date()
        let since1970 = currentDate.timeIntervalSince1970
        return Int(since1970 * 1000)
    }
    
    private func parseBoolean(data: String?, defaultValue: Bool) -> Bool {
        var result: Bool
        
        if let unwrappeData = data {
            result = NSString(string: unwrappeData).boolValue
        } else {
            result = defaultValue
        }
        return result
    }
    
    public func hmac(string: String, key: String) -> String {
        let cKey = key.cString(using: String.Encoding.ascii)
        let cData = string.cString(using: String.Encoding.ascii)
        var result = [CUnsignedChar](unsafeUninitializedCapacity: Int(CC_SHA256_DIGEST_LENGTH), initializingWith: {_,_ in})
        CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA256), cKey!, strlen(cKey!), cData!, strlen(cData!), &result)
        let hmacData:NSData = NSData(bytes: result, length: (Int(CC_SHA256_DIGEST_LENGTH)))
        let hmacBase64 = hmacData.base64EncodedString()
        return String(hmacBase64)
    }
    
}

