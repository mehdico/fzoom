class Constants {
  static const APP_KEY = "";
  static const APP_SECRET = "";
  static const API_KEY = "";
  static const API_SECRET = "";
  static const DOMAIN = "zoom.us";
}
