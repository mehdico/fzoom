package com.gitlab.mehdico.fzoom;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.gitlab.mehdico.fzoom.apiuser.APIUserInfo;
import com.gitlab.mehdico.fzoom.apiuser.APIUserInfoHelper;
import com.gitlab.mehdico.fzoom.helpers.OnTaskCompleted;
import io.flutter.Log;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.platform.PlatformView;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import us.zoom.androidlib.util.StringUtil;
import us.zoom.sdk.InMeetingService;
import us.zoom.sdk.InstantMeetingOptions;
import us.zoom.sdk.JoinMeetingOptions;
import us.zoom.sdk.JoinMeetingParams;
import us.zoom.sdk.MeetingService;
import us.zoom.sdk.MeetingStatus;
import us.zoom.sdk.StartMeetingOptions;
import us.zoom.sdk.StartMeetingParamsWithoutLogin;
import us.zoom.sdk.ZoomApiError;
import us.zoom.sdk.ZoomError;
import us.zoom.sdk.ZoomSDK;
import us.zoom.sdk.ZoomSDKAuthenticationListener;
import us.zoom.sdk.ZoomSDKInitParams;
import us.zoom.sdk.ZoomSDKInitializeListener;

public class ZoomView implements PlatformView,
    MethodChannel.MethodCallHandler,
    ZoomSDKAuthenticationListener {
   private final TextView textView;
   private final MethodChannel methodChannel;
   private final Context context;
   private final EventChannel meetingStatusChannel;
   private MethodChannel.Result flutterResult;
   private static final  String TAG = "_FZOOM_";

   private static final int ERROR_INIT_FAILED = -1;
   private static final int ERROR_SERVICE_NULL = -2;
   private static final int ERROR_USER_INFO_NULL = -3;
   private static final int ERROR_MEETING_NOT_FOUND = -6;
   private static final int SUCCESS = 0;

   ZoomView(Context context, BinaryMessenger messenger, int id) {
      textView     = new TextView(context);
      this.context = context;

      methodChannel = new MethodChannel(messenger, "zoom_view");
      methodChannel.setMethodCallHandler(this);

      meetingStatusChannel = new EventChannel(messenger, "zoom_event_stream_" + id);
   }

   @Override
   public View getView() {
      return textView;
   }

   @Override
   public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
      Log.i(TAG, "android methodcall: " + methodCall.method);
      switch (methodCall.method) {
         case "init":
            init(methodCall, result);
            break;
         case "joinMeetingNoLogin":
            joinMeetingNoLogin(methodCall, result);
            break;
         case "login":
            login(methodCall, result);
            break;
         case "logout":
            logout(methodCall, result);
            break;
         case "startMeeting":
            startMeeting(methodCall, result);
            break;
         case "startMeetingNoLogin":
            startMeetingNoLogin(methodCall, result);
            break;
         case "meetingStatus":
            meetingStatus(result);
            break;
         case "currentMeetingInviteInfo":
            currentMeetingInviteInfo(result);
            break;
         default:
            result.notImplemented();
      }
   }

   private void init(final MethodCall methodCall, final MethodChannel.Result result) {

      Map<String, String> options = methodCall.arguments();

      ZoomSDK zoomSDK = ZoomSDK.getInstance();

      if (zoomSDK.isInitialized()) {
         MeetingService meetingService = zoomSDK.getMeetingService();
         meetingStatusChannel.setStreamHandler(new StatusStreamHandler(meetingService));
         result.success(Arrays.asList(0, 0));
         return;
      }

      ZoomSDKInitParams initParams = new ZoomSDKInitParams();
      initParams.appKey    = options.get("appKey");
      initParams.appSecret = options.get("appSecret");
      initParams.domain    = options.get("domain");

      zoomSDK.initialize(
          context,
          new ZoomSDKInitializeListener() {

             @Override
             public void onZoomAuthIdentityExpired() {

             }

             @Override
             public void onZoomSDKInitializeResult(int errorCode, int internalErrorCode) {
                List<Integer> response = Arrays.asList(errorCode, internalErrorCode);

                if (errorCode != ZoomError.ZOOM_ERROR_SUCCESS) {
                   Log.e(TAG, "Failed to initialize Zoom SDK");
                   result.success(response);
                   return;
                }

                ZoomSDK.getInstance().addAuthenticationListener(ZoomView.this);

                ZoomSDK zoomSDK = ZoomSDK.getInstance();
                MeetingService meetingService = zoomSDK.getMeetingService();
                meetingStatusChannel.setStreamHandler(new StatusStreamHandler(meetingService));
                result.success(response);
             }
          },
          initParams);
   }

   private void joinMeetingNoLogin(MethodCall methodCall, MethodChannel.Result result) {

      Map<String, String> options = methodCall.arguments();

      ZoomSDK zoomSDK = ZoomSDK.getInstance();

      if (!zoomSDK.isInitialized()) {
         result.success(ERROR_INIT_FAILED);
         return;
      }

      MeetingService meetingService = zoomSDK.getMeetingService();
      if (meetingService == null) {
         Log.e(TAG, "meetingService is null");
         result.success(ERROR_SERVICE_NULL);
         return;
      }

      JoinMeetingOptions opts = new JoinMeetingOptions();
      opts.no_invite            = parseBoolean(options, "disableInvite", false);
      opts.no_share             = parseBoolean(options, "disableShare", false);
      opts.no_driving_mode      = parseBoolean(options, "disableDrive", false);
      opts.no_dial_in_via_phone = parseBoolean(options, "disableDialIn", false);
      opts.no_disconnect_audio  = parseBoolean(options, "noDisconnectAudio", false);
      opts.no_audio             = parseBoolean(options, "noAudio", false);

      JoinMeetingParams params = new JoinMeetingParams();

      params.displayName = options.get("displayName");
      params.meetingNo   = options.get("meetingId");
      params.password    = options.get("meetingPassword");

      int resp = meetingService.joinMeetingWithParams(context, params, opts);

      result.success(resp);
   }

   private void login(MethodCall methodCall, MethodChannel.Result result) {
      Map<String, String> options = methodCall.arguments();

      ZoomSDK zoomSDK = ZoomSDK.getInstance();

      if (!zoomSDK.isInitialized()) {
         result.success(ERROR_INIT_FAILED);
         return;
      }

      //todo move all validations to dart side of the project!!!
      String email = options.get("userId");
      String password = options.get("password");
      if (StringUtil.isEmptyOrNull(email) || StringUtil.isEmptyOrNull(password)) {
         Toast.makeText(context, "You need to enter user name and password.", Toast.LENGTH_LONG).show();
         return;
      }

      zoomSDK.removeAuthenticationListener(this);
      zoomSDK.addAuthenticationListener(this);

      int resp = zoomSDK.loginWithZoom(email, password);
      if (resp != ZoomApiError.ZOOM_API_ERROR_SUCCESS) {
         result.success(resp);
         return;
      }

      flutterResult = result;
   }

   private void logout(MethodCall methodCall, MethodChannel.Result result) {
      ZoomSDK zoomSDK = ZoomSDK.getInstance();
      if (!zoomSDK.isInitialized()) {
         result.success(ERROR_INIT_FAILED);
         return;
      }

      if (!zoomSDK.isLoggedIn()) {
         result.success(SUCCESS);
         return;
      }

      zoomSDK.removeAuthenticationListener(this);
      zoomSDK.addAuthenticationListener(this);

      flutterResult = result;
      zoomSDK.logoutZoom();
   }

   private void startMeeting(MethodCall methodCall, final MethodChannel.Result result) {
      Map<String, String> options = methodCall.arguments();

      ZoomSDK zoomSDK = ZoomSDK.getInstance();
      if (!zoomSDK.isInitialized()) {
         result.success(ERROR_INIT_FAILED);
         return;
      }

      MeetingService meetingService = zoomSDK.getMeetingService();
      if (meetingService == null) {
         Log.e(TAG, "meetingService is null");
         result.success(ERROR_SERVICE_NULL);
         return;
      }

      InstantMeetingOptions opts = new InstantMeetingOptions();
      opts.no_invite            = parseBoolean(options, "disableInvite", false);
      opts.no_share             = parseBoolean(options, "disableShare", false);
      opts.no_driving_mode      = parseBoolean(options, "disableDrive", false);
      opts.no_dial_in_via_phone = parseBoolean(options, "disableDialIn", false);
      opts.no_disconnect_audio  = parseBoolean(options, "noDisconnectAudio", false);
      opts.no_video             = parseBoolean(options, "noVideo", false);

      int resp = meetingService.startInstantMeeting(context, opts);
      Log.i(TAG, "startInstantMeeting result is " + resp);

      result.success(resp);
   }

   private void startMeetingNoLogin(final MethodCall methodCall, final MethodChannel.Result result) {

      final Map<String, String> options = methodCall.arguments();
      final StartMeetingOptions opts = new StartMeetingOptions();
      opts.no_invite            = parseBoolean(options, "disableInvite", false);
      opts.no_share             = parseBoolean(options, "disableShare", false);
      opts.no_driving_mode      = parseBoolean(options, "disableDrive", false);
      opts.no_dial_in_via_phone = parseBoolean(options, "disableDialIn", false);
      opts.no_disconnect_audio  = parseBoolean(options, "noDisconnectAudio", false);
      opts.no_video             = parseBoolean(options, "noVideo", false);

      RetrieveUserInfoTask task = new RetrieveUserInfoTask(new OnTaskCompleted() {
         @Override public void onRetrieveUserInfoTaskCompleted() {

            APIUserInfo userInfo = APIUserInfoHelper.getAPIUserInfo();
            if (userInfo == null) {
               Log.e(TAG, "userInfo is null");
               result.success(ERROR_USER_INFO_NULL);
               return;
            }
            ZoomSDK zoomSDK = ZoomSDK.getInstance();

            if (!zoomSDK.isInitialized()) {
               result.success(ERROR_INIT_FAILED);
               return;
            }

            MeetingService meetingService = zoomSDK.getMeetingService();
            if (meetingService == null) {
               Log.e(TAG, "meetingService is null");
               result.success(ERROR_SERVICE_NULL);
               return;
            }

            StartMeetingParamsWithoutLogin params = new StartMeetingParamsWithoutLogin();
            params.userId          = userInfo.userId;
            params.userType        = MeetingService.USER_TYPE_API_USER;
            params.displayName     = options.get("displayName");
            params.meetingNo       = options.get("meetingId");
            params.zoomToken       = userInfo.userZoomToken;
            params.zoomAccessToken = userInfo.userZoomAccessToken;

            int resp = meetingService.startMeetingWithParams(context, params, opts);
            Log.i(TAG, "startMeetingWithNumber, ret=" + resp);

            result.success(resp);
         }
      });//retrieve api user token
      task.execute(options.get("userId"), options.get("apiKey"), options.get("apiSecret"));
   }

   private void currentMeetingInviteInfo(final MethodChannel.Result result) {

      ZoomSDK zoomSDK = ZoomSDK.getInstance();

      if (!zoomSDK.isInitialized()) {
         result.success(Arrays.asList(ERROR_INIT_FAILED));
         return;
      }

      InMeetingService inMeetingService = zoomSDK.getInMeetingService();
      if (inMeetingService == null) {
         Log.e(TAG, "inMeetingService is null");
         result.success(Arrays.asList(ERROR_SERVICE_NULL));
         return;
      }

      if(!inMeetingService.isMeetingConnected()) {
         result.success(Arrays.asList(ERROR_MEETING_NOT_FOUND));
         return;
      }
      result.success(Arrays.asList(
          SUCCESS,
          inMeetingService.getCurrentMeetingNumber(),
          inMeetingService.getMeetingPassword(),
          inMeetingService.getCurrentMeetingUrl()
      ));
   }

   private boolean parseBoolean(Map<String, String> options, String property, boolean defaultValue) {
      return options.get(property) == null ? defaultValue : Boolean.parseBoolean(options.get(property));
   }

   private void meetingStatus(MethodChannel.Result result) {

      ZoomSDK zoomSDK = ZoomSDK.getInstance();

      if (!zoomSDK.isInitialized()) {
         result.success(Arrays.asList("MEETING_STATUS_UNKNOWN", "SDK not initialized"));
         return;
      }

      MeetingService meetingService = zoomSDK.getMeetingService();

      if (meetingService == null) {
         result.success(Arrays.asList("MEETING_STATUS_UNKNOWN", "No status available"));
         return;
      }

      MeetingStatus status = meetingService.getMeetingStatus();
      result.success(status != null ? Arrays.asList(status.name(), "") : Arrays.asList("MEETING_STATUS_UNKNOWN", "No status available"));
   }

   @Override
   public void dispose() {
   }

   @Override
   public void onZoomAuthIdentityExpired() {

   }

   @Override
   public void onZoomSDKLoginResult(long result) {
      if (flutterResult != null) {
         flutterResult.success(result);
         flutterResult = null;
      }
   }

   @Override
   public void onZoomSDKLogoutResult(long result) {
      if (flutterResult != null) {
         flutterResult.success(result);
         flutterResult = null;
      }
   }

   @Override
   public void onZoomIdentityExpired() {

   }

   private class RetrieveUserInfoTask extends AsyncTask<String, Void, APIUserInfo> {

      private OnTaskCompleted listener;

      public RetrieveUserInfoTask(OnTaskCompleted listener) {
         this.listener = listener;
      }

      @Override
      protected void onPreExecute() {
         super.onPreExecute();
      }

      /*
       * params[0] userId
       * params[1] apiKey
       * params[2] apiSecret
       * */
      @Override
      protected APIUserInfo doInBackground(String... params) {
         String token = APIUserInfoHelper.getZoomToken(params[0], params[1], params[2]);
         String accessToken = APIUserInfoHelper.getZoomAccessToken(params[0], params[1], params[2]);
         if (token != null && !token.isEmpty() && accessToken != null && !accessToken.isEmpty()) {
            APIUserInfo apiUserInfo = new APIUserInfo(params[0], token, accessToken);
            APIUserInfoHelper.saveAPIUserInfo(apiUserInfo);
            return apiUserInfo;
         } else {
            return null;
         }
      }

      @Override
      protected void onPostExecute(APIUserInfo apiUserInfo) {
         super.onPostExecute(apiUserInfo);
         if (apiUserInfo == null) {
            Log.e(TAG, "Faild to retrieve Api user info!");
         }
         listener.onRetrieveUserInfoTaskCompleted();
      }
   }
}
